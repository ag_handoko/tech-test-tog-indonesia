import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import Create from "./component/pages/create";
import Detail from "./component/pages/view";
import Edit from "./component/pages/edit";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Router>
    <Routes>
      <Route path="/" element={<App />}></Route>
      <Route path="/create" element={<Create />}></Route>
      <Route path="/view" element={<Detail />}></Route>
      <Route path="/edit" element={<Edit />}></Route>
    </Routes>
  </Router>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
