import React from "react";
import { Container } from "react-bootstrap";
import { BsBriefcaseFill, BsFillHouseDoorFill } from "react-icons/bs";
import { Link } from "react-router-dom";
import style from "./style.module.css";

const Sidebar = () => {
  return (
    <Container className={style.sidebar}>
      <ul className="nav-link">
        <li className="nav-item">
          <Link to="#" rel="noopener">
            <BsFillHouseDoorFill />
          </Link>
        </li>
        <li className="nav-item active">
          <Link to="#" rel="noopener">
            <BsBriefcaseFill />
          </Link>
        </li>
      </ul>
    </Container>
  );
};

export default Sidebar;
