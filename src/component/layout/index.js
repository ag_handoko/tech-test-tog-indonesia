import React from "react";
import Header from "./header";
import Sidebar from "./sidebar";
import { Row, Col, Breadcrumb } from "react-bootstrap";
import { Link, useLocation } from "react-router-dom";
import Footer from "./footer";

const Layout = ({ children }) => {
  const location = useLocation();
  const titlePage =
    location.pathname.length > 1
      ? location.pathname.split("/")[1] + " fee type"
      : "fee type";

  return (
    <div className="layout">
      <Row
        style={{
          height: "100%",
          marginRight: 0,
        }}
      >
        <Col xs="1" style={{ paddingRight: 0 }}>
          <Sidebar />
        </Col>
        <Col
          style={{
            paddingLeft: 0,
            paddingRight: 0,
            display: "inline-flex",
            flexDirection: "column",
          }}
        >
          <Header />
          <div className="content">
            <Breadcrumb>
              <li className="breadcrumb-item">
                <Link to="/">Master Data Management</Link>
              </li>
              {location.pathname.length > 1 ? (
                <>
                  <li className="breadcrumb-item">
                    <Link to="/">Fee Type</Link>
                  </li>
                  <Breadcrumb.Item
                    active
                    style={{ textTransform: "capitalize" }}
                  >
                    {location.pathname.split("/")[1] === "view"
                      ? "Fee Type Details"
                      : titlePage}
                  </Breadcrumb.Item>
                </>
              ) : (
                <Breadcrumb.Item active>Fee Type</Breadcrumb.Item>
              )}
            </Breadcrumb>
            <h1 className="title">
              {location.pathname.split("/")[1] === "view"
                ? "Fee Type Details"
                : titlePage}
            </h1>
            {children}
          </div>
          <Footer />
        </Col>
      </Row>
    </div>
  );
};

export default Layout;
