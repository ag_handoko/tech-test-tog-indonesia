import React from "react";
import style from "./style.module.css";

const Footer = () => {
  return (
    <div className={style.footer}>
      © 2020 Bayu Buana Travel Services. All Rights Reserved.
    </div>
  );
};

export default Footer;
