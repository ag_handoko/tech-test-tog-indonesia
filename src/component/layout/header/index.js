import React from "react";
import { Col, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import { BsFillQuestionCircleFill, BsBell } from "react-icons/bs";
import style from "./style.module.css";

const Header = () => {
  return (
    <div className={style.header}>
      <Link to="/">
        <img
          style={{ maxWidth: 150 }}
          src="img/bayu_buana_logo.png"
          alt="logo"
        />
      </Link>
      <div className="ml-auto">
        <Row className="align-center">
          <Col xs="3">
            <BsFillQuestionCircleFill style={{ cursor: "pointer" }} />
          </Col>
          <Col xs="3" className={style.notification}>
            <span className={style.alertNotif}></span>
            <BsBell style={{ cursor: "pointer" }} />
          </Col>
          <Col xs="6" style={{ cursor: "pointer" }}>
            <img
              className={style.profile}
              src="img/profile.jpg"
              alt="profile"
            />
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default Header;
