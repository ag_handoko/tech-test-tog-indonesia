import React from "react";
import Layout from "../layout";
import { Formik, Form as FormikForm, Field } from "formik";
import { Col, Row, Form } from "react-bootstrap";
import {
  BsFillExclamationTriangleFill,
  BsExclamationCircle,
} from "react-icons/bs";
import { Link } from "react-router-dom";
import Validation from "./validate";

const Detail = () => {
  // const [formData, setFormData] = React.useState({
  // 	name: '',
  // 	desc: '',
  // 	code: '',
  // });

  return (
    <Layout>
      <Formik
        initialValues={{
          name: "Service Fee",
          desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sollicitudin turpis vel enim efficitur interdum. Etiam euismod nunc vel justo pretium, et pulvinar dolor malesuada.",
          code: 1,
          translation: "Indonesia",
        }}
        validationSchema={Validation}
        onSubmit={(values) => {
          // same shape as initial values
          console.log(values);
        }}
      >
        {({ errors, touched }) => (
          <FormikForm disabled>
            <div className="form-group">
              <Row>
                <Col sm="7">
                  <Form.Group as={Row} className="mb-3" controlId="name">
                    <Form.Label column sm={4}>
                      Fee Type Name
                    </Form.Label>
                    <Col sm={6}>
                      <Field className="form-control" name="name" disabled />
                      {errors.name && touched.name ? (
                        <div className="error-validation">
                          <BsFillExclamationTriangleFill
                            style={{ color: "#FF0000", marginRight: 6 }}
                          />
                          {errors.name}
                        </div>
                      ) : null}
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row} className="mb-3" controlId="desc">
                    <Form.Label column sm={4}>
                      Description
                    </Form.Label>
                    <Col sm={6}>
                      <Field
                        className="form-control"
                        name="desc"
                        as="textarea"
                        rows="6"
                        disabled
                      />
                    </Col>
                  </Form.Group>
                </Col>
                <Col sm="5" className="form-box">
                  <b className="mb-2">For Interface Purpose</b>
                  <Form.Group as={Row} controlId="code">
                    <Form.Label column sm={4} style={{ display: "flex" }}>
                      <div style={{ marginRight: 2 }}>Fee Type Code</div>
                      <BsExclamationCircle
                        style={{
                          color: "#818181",
                          width: 10,
                          height: 10,
                          cursor: "pointer",
                        }}
                      />
                    </Form.Label>
                    <Col sm={5}>
                      <Field className="form-control" name="code" disabled />
                      {errors.code && touched.code ? (
                        <div className="error-validation">
                          <BsFillExclamationTriangleFill
                            style={{ color: "#FF0000", marginRight: 6 }}
                          />
                          {errors.code}
                        </div>
                      ) : null}
                    </Col>
                  </Form.Group>
                </Col>
              </Row>
              <div className="mt-5">
                <h5>Translation</h5>
                <hr />
              </div>
              <div className="mt-3">
                <Field
                  className="form-control mb-2"
                  name="translation"
                  placeholder="Indonesia"
                  disabled
                />
                <div
                  className="form-control"
                  style={{ display: "inline-flex", backgroundColor: "#ECECEC" }}
                >
                  Chinese Simplified
                  <BsFillExclamationTriangleFill
                    style={{
                      color: "#FF0000",
                      width: 12,
                      height: 12,
                      marginLeft: 4,
                    }}
                  />
                </div>
              </div>
              <div className="mt-3 note">
                <span>Note </span>
                <BsFillExclamationTriangleFill
                  style={{ color: "#FF0000" }}
                />{" "}
                Incomplete data
              </div>
            </div>
            <div className="mt-4">
              <Link to="/" className="btn btn-buana-cancel">
                Back
              </Link>
            </div>
          </FormikForm>
        )}
      </Formik>
    </Layout>
  );
};

export default Detail;
