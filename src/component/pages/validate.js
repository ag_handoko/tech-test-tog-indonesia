import * as yup from "yup";

const numberRegExp = /^(0|[1-9][0-9]*)$/;

const Validation = yup.object().shape({
  name: yup.string().required("Required"),
  desc: yup.string(),
  code: yup
    .string()
    .required("Required")
    .matches(numberRegExp, "Only numbers are allowed for this field "),
  translation: yup.string(),
});

export default Validation;
