import React from 'react';
import Layout from './component/layout';
import { Link, useNavigate } from 'react-router-dom';
import {
	BsEyeFill,
	BsPencilSquare,
	BsTrashFill,
	BsThreeDotsVertical,
} from 'react-icons/bs';
import { Form } from 'react-bootstrap';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'datatables.net-dt/css/jquery.dataTables.min.css';

import 'jquery/dist/jquery.min.js';
import 'datatables.net-dt/js/dataTables.dataTables';
import 'datatables.net-buttons/js/dataTables.buttons.js';
import 'datatables.net-buttons/js/buttons.colVis.js';
import 'datatables.net-buttons/js/buttons.flash.js';
import 'datatables.net-buttons/js/buttons.html5.js';
import 'datatables.net-buttons/js/buttons.print.js';
import $ from 'jquery';

const App = () => {
	const navigate = useNavigate();
	const [editMode, setEditMode] = React.useState(false);
	const data = [
		{
			name: 'Service Fee',
			desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sollicitudin turpis vel enim efficitur interdum. Etiam euismod nunc vel justo pretium, et pulvinar dolor malesuada.',
			status: 0,
			translation: 'Indonesia',
		},
		{
			name: 'Reissue Fee',
			desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sollicitudin turpis vel enim efficitur interdum.',
			status: 1,
			translation: 'Indonesia',
		},
		{
			name: 'Cancellation Fee',
			desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sollicitudin turpis vel enim efficitur interdum.',
			status: 1,
			translation: 'Indonesia',
		},
		{
			name: 'Reissue Fee',
			desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sollicitudin turpis vel enim efficitur interdum.',
			status: 1,
			translation: 'Indonesia',
		},
		{
			name: 'MDR Fee',
			desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sollicitudin turpis vel enim efficitur interdum.',
			status: 1,
			translation: 'Indonesia',
		},
		{
			name: 'After Office Charge',
			desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sollicitudin turpis vel enim efficitur interdum.',
			status: 1,
			translation: 'Indonesia',
		},
		{
			name: 'Late Payment',
			desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sollicitudin turpis vel enim efficitur interdum.',
			status: 1,
		},
		{
			name: 'Domestic Flight Service Fee',
			desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sollicitudin turpis vel enim efficitur interdum.',
			status: 1,
			translation: 'Indonesia',
		},
		{
			name: 'International Flight Service Fee',
			desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sollicitudin turpis vel enim efficitur interdum.',
			status: 1,
			translation: 'Indonesia',
		},
		{
			name: 'Domestic Hotel Service Fee',
			desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sollicitudin turpis vel enim efficitur interdum.',
			status: 1,
			translation: 'Indonesia',
		},
		{
			name: 'Domestic Hotel Service Fee',
			desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sollicitudin turpis vel enim efficitur interdum.',
			status: 1,
			translation: 'Indonesia',
		},
	];

	React.useEffect(() => {
		if (!$.fn.DataTable.isDataTable('#myTable')) {
			$(document).ready(function () {
				setTimeout(function () {
					let table = $('#table').DataTable({
						pageLength: 10,
						order: [],
						processing: true,
						dom: "<<'top-wrapper'<'query'f<'adv'>>B><'edit-wrapper'><t><'pagination'<'info'li>p>>",
						select: true,
						language: {
							paginate: {
								previous:
									'<svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 16 16" height="15" width="15" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"></path></svg>',
								next: '<svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 16 16" height="15" width="15" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></svg>',
							},
							lengthMenu: '_MENU_',
							search:
								'_INPUT_ <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 16 16" height="15" width="15" xmlns="http://www.w3.org/2000/svg"><path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"></path></svg>',
							searchPlaceholder: 'Search...',
							info: 'Showing _START_ - _END_ of _TOTAL_',
						},
						buttons: [
							{
								extend: 'csv',
								className: 'btn custom-button mr-2',
								text: '<svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 16 16" height="15" width="15" xmlns="http://www.w3.org/2000/svg"><path d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"></path><path d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z"></path></svg>',
							},
							{
								extend: 'print',
								customize: function (win) {
									$(win.document.body).css('font-size', '10pt');
									$(win.document.body)
										.find('table')
										.addClass('compact')
										.css('font-size', 'inherit');
								},
								className: 'btn custom-button mr-2',
								text: '<svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 16 16" height="15" width="15" xmlns="http://www.w3.org/2000/svg"><path d="M2.5 8a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1z"></path><path d="M5 1a2 2 0 0 0-2 2v2H2a2 2 0 0 0-2 2v3a2 2 0 0 0 2 2h1v1a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2v-1h1a2 2 0 0 0 2-2V7a2 2 0 0 0-2-2h-1V3a2 2 0 0 0-2-2H5zM4 3a1 1 0 0 1 1-1h6a1 1 0 0 1 1 1v2H4V3zm1 5a2 2 0 0 0-2 2v1H2a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1h-1v-1a2 2 0 0 0-2-2H5zm7 2v3a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1v-3a1 1 0 0 1 1-1h6a1 1 0 0 1 1 1z"></path></svg>',
							},
							{
								text: '<svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 16 16" height="15" width="15" xmlns="http://www.w3.org/2000/svg"><path d="M8 6.5a.5.5 0 0 1 .5.5v1.5H10a.5.5 0 0 1 0 1H8.5V11a.5.5 0 0 1-1 0V9.5H6a.5.5 0 0 1 0-1h1.5V7a.5.5 0 0 1 .5-.5z"></path><path d="M14 4.5V14a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h5.5L14 4.5zm-3 0A1.5 1.5 0 0 1 9.5 3V1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V4.5h-2z"></path></svg>Create New',
								className: 'btn add-btn',
								action: () => navigate('/create'),
							},
						],
						columnDefs: [
							{
								targets: [0, 5],
								orderable: false,
								render: function (data, type, row, meta) {
									return type === 'export' ? meta.row + 1 : data;
								},
							},
						],
					});

					new $.fn.dataTable.Buttons(table, {
						buttons: [
							{
								text: 'UPDATE STATUS <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 16 16" height="12" width="12" xmlns="http://www.w3.org/2000/svg"><path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"></path></svg>',
								className: 'btn btn btn-buana-save mr-2',
								action: function (e, dt, node, conf) {
									alert('Button 2 clicked on');
								},
							},
							{
								text: 'REMOVE FEE TYPE',
								className: 'btn btn btn-buana-save',
								action: function (e, dt, node, conf) {
									alert('Button 3 clicked on');
								},
							},
						],
					});

					table.buttons(1, null).container().appendTo('.edit-wrapper');
				}, 1000);
			});
		}

		// return () => {
		//   second
		// }
	}, [navigate]);

	React.useEffect(() => {
		setTimeout(() => {
			if ($('.query')) {
				$('.adv').append(`
          Advanced Options
          <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 16 16" height="13" width="13" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M1.646 6.646a.5.5 0 0 1 .708 0L8 12.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"></path><path fill-rule="evenodd" d="M1.646 2.646a.5.5 0 0 1 .708 0L8 8.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"></path></svg>
        `);
			}
		}, 1100);

		if ($('.edit-wrapper')) {
			if (editMode) {
				$('.edit-wrapper').css('display', 'block');
				// $(".edit-wrapper").show();
			} else {
				$('.edit-wrapper').css('display', 'none');
			}
		}

		// return () => {
		//   setEditMode(false);
		// };
	}, [editMode]);

	const showTable = () => {
		try {
			return data.map((item, index) => {
				return (
					<tr key={index}>
						<td>
							<div className='d-flex align-items-center'>
								<BsThreeDotsVertical
									style={{
										width: 12,
										height: 12,
										marginRight: 10,
										color: '#666666',
									}}
								/>
								<Form.Check
									type='checkbox'
									id='check-all'
									value={editMode}
									onChange={(e) =>
										e.target.checked !== editMode && setEditMode(!editMode)
									}
								/>
							</div>
						</td>
						<td className='text-xs font-weight-bold'>{index + 1}</td>
						<td className='text-xs font-weight-bold'>{item.name}</td>
						<td className='text-xs font-weight-bold' style={{ maxWidth: 300 }}>
							{item.desc}
						</td>
						<td className='text-xs font-weight-bold'>
							{item.status === 0 ? 'Inative' : 'Active'}
						</td>
						<td className='text-xs font-weight-bold'>
							<div className='d-flex align-items-center actions justify-content-center'>
								<Link to='/edit' className='mr-2'>
									<BsPencilSquare style={{ width: 18, height: 18 }} />
								</Link>
								<Link to='/view' className='mr-2'>
									<BsEyeFill style={{ width: 18, height: 18 }} />
								</Link>
								<div>
									<BsTrashFill
										style={{
											width: 16,
											height: 16,
											color: '#3E40AE',
											cursor: 'pointer',
										}}
									/>
								</div>
							</div>
						</td>
					</tr>
				);
			});
		} catch (e) {
			// alert(e.message);
		}
	};

	return (
		<Layout>
			<div className='container-fluid py-4'>
				<div className='table-responsive p-0 pb-2'>
					<table
						id='table'
						className='table align-items-center justify-content-center mb-0'>
						<thead>
							<tr>
								<th style={{ textAlign: 'center' }}>
									<Form.Check
										type='checkbox'
										id='check-all'
										value={editMode}
										onChange={(e) =>
											e.target.checked !== editMode && setEditMode(!editMode)
										}
									/>
								</th>
								<th>Fee Type Code</th>
								<th>Fee Type Name</th>
								<th>Description</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						</thead>

						<tbody>{showTable()}</tbody>
					</table>
				</div>
			</div>
		</Layout>
	);
};

export default App;
